import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { TodoServiceService } from '../todo-service.service';
import { Todo } from '../todo.models';
import * as fromTodoReducers from '../todo.reducers';
import * as fromTodoActions from '../todo.action';
import { select, Store } from '@ngrx/store';
import * as fromTodoSelectors from '../todo.selectors';


@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent {
  todos: Observable<Todo[]>;
  count$: Observable<number>;
  isEdit = false;
  name!: string;
  selectedTodo!: Todo;
  
  constructor(private todoService: TodoServiceService, private store: Store<fromTodoReducers.State>) { 
    this.todos = store.pipe(select(fromTodoSelectors.selecttAll));
    this.count$ = store.pipe(select(fromTodoSelectors.selectTotal));
  }


  addTodo(name: string): void {
    const todo: Todo = new Todo(name);

    //this.store.dispatch( {type: 'ADD_TODO', payload: todo });
    this.store.dispatch(new fromTodoActions.AddTodo(todo));
    this.name = '';
  }


  updateTodo(todo: Todo): void {
    this.isEdit = true;
    this.name = todo.name;
    this.selectedTodo = todo;
  }

  confirmTodo(name: string): void {
    this.selectedTodo = { ...this.selectedTodo, name };
    //this.store.dispatch( {type: 'UPDATE_TODO', payload: this.selectedTodo });
    this.store.dispatch( new fromTodoActions.ApdateTodo(this.selectedTodo));
    this.isEdit = false;
    this.name = '';
  }


  deleteTodo(todo: Todo): void {
 // this.store.dispatch( {type: 'DELETE_TODO', payload: todo.id });
 this.store.dispatch( new fromTodoActions.DeleteTodo(todo.id));

  }

}
