import { Action } from "@ngrx/store";
import { TodoActions, TodoActionsTypes } from "./todo.action";
import { Todo } from "./todo.models";

export interface State {
     todos: Todo[];
     lastUpdate: string;
}

//Default data 
const initialState: State = {
    todos: [
                new Todo('Learn NgRx', 1),
                new Todo('Learn Angular', 2),
                new Todo('Learn React', 3),
                new Todo('Learn Scala', 4),          
                ],

                lastUpdate: new Date().toString()
};

export default function todoReducer(state = initialState, action: any /*TodoActions*/): State {

    switch(action.type) {
        case TodoActionsTypes.ADD_TODO:
            return {
                    ...state,
                    lastUpdate: new Date().toString(),
                    todos: [...state.todos, action.payload]
                  };

                  case TodoActionsTypes.DELETE_TODO:
            return {
                    ...state,
                    lastUpdate: new Date().toString(),
                    todos: [...state.todos].filter((t: Todo)=> t.id !== action.payload)
                  };

                  case TodoActionsTypes?.DELETE_ALL_TODOS:
            return {
                    ...state,
                    lastUpdate: new Date().toString(),
                    todos: []
                  };

                  case TodoActionsTypes.UPDATE_TODO:
                      const todos = state.todos.map((t: Todo) => {
                          if (t.id === action.payload.id) {
                              t = {  ...t, ...action.payload }
                              } 
                              return t;
                          });
                          
                      
                    return {
                            ...state,
                            todos,
                            lastUpdate: new Date().toString(),
                          };
            
             default: 

                 return state;

             
         
    }

}