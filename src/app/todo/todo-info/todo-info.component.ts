import { Component } from '@angular/core';
import { select, Store } from '@ngrx/store';

import { Observable } from 'rxjs';
import * as fromTodoReducers from '../todo.reducers';
import * as fromTodoActions from '../todo.action';
import * as fromTodoSelectors from '../todo.selectors';


@Component({
  selector: 'app-todo-info',
  templateUrl: './todo-info.component.html',
})
export class TodoInfoComponent {
  //todosState$: Observable<fromTodoReducers.State>;
  count$: Observable<number>;
  lastUpdate$: Observable<string>;

  constructor(private store: Store<fromTodoReducers.State>) {
     this.count$ = store.pipe(select(fromTodoSelectors.selectTotal));
     this.lastUpdate$ = store.pipe(select(fromTodoSelectors.selectLastUpdate));
  }

  deleteAllTodos(): void {
    //this.todoService.deleteAll();
    this.store.dispatch (new fromTodoActions.DeleteAllTodos());
  }

}
