import { createFeatureSelector, createSelector } from "@ngrx/store";
import { Todo } from "./todo.models";
import { State } from "./todo.reducers";

export const getTodoState = createFeatureSelector<State>('todo');
export const selecttAll = createSelector(getTodoState, (state: State): Todo[] => state.todos);
export const selectTotal = createSelector(getTodoState, (state: State): number => state.todos.length);
export const selectLastUpdate = createSelector(getTodoState, (state: State): string => state.lastUpdate);
