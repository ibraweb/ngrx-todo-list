import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { TodoListComponent } from './todo/todo-list/todo-list.component';
import { TodoInfoComponent } from './todo/todo-info/todo-info.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import {  StoreDevtoolsModule } from '@ngrx/store-devtools';
import  todoReducer  from './todo/todo.reducers';


@NgModule({
  declarations: [
    AppComponent,
    TodoListComponent,
    TodoInfoComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    StoreModule.forRoot( {todo: todoReducer }),
    //!environment.production ? StoreDevtoolsModule, instrument( {maxAge: 5}) : [],
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
